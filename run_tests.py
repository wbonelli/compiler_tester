#!/usr/bin/env python

import argparse
from dataclasses import dataclass
import glob
import os

import toml

@dataclass
class TestResult:
    ast: bool = False
    asr: bool = False
    llvm: bool = False
    bin: bool = False
    run: bool = False

@dataclass
class Test:
    filename: str
    description: str
    result: TestResult = None

@dataclass
class Feature:
    name: str
    description: str
    directory: str
    tests: list[Test]

@dataclass
class Topic:
    name: str
    description: str
    features: list[Feature]

@dataclass
class TestSuite:
    topics: list[Topic]

def collect_tests(d):
    topics = []
    for topic_name in d:
        topic0 = d[topic_name]
        features = []
        for test_name in topic0:
            test0 = topic0[test_name]
            if isinstance(test0, dict):
                directory = "tests" + "/" + topic_name + "/" + test_name
                if not os.path.isdir(directory):
                    raise Exception("Directory '%s' does not exist" \
                            % directory)
                description = test0.get("description", "")
                tests = []
                g = glob.glob(directory+"/*.f90")
                g.sort()
                for filename in g:
                    first_line = open(filename).readline()
                    test_description = None
                    if first_line.startswith("!"):
                        if len(first_line) > 2:
                            if first_line[1] == " ":
                                test_description = first_line[2:].rstrip("\n")
                            else:
                                raise Exception("Second character must be space in the first line")
                        else:
                            raise Exception("First line too short")
                    else:
                        raise Exception("The first line must be a '!' style comment with the one line test description")
                    tests.append(Test(filename=filename,
                        description=test_description))
                features.append(Feature(name=test_name, description=description,
                    directory=directory, tests=tests))
        if "description" not in topic0:
            raise Exception("Topic '%s' does not have a description" \
                    % topic_name)
        topics.append(Topic(name=topic_name, description=topic0["description"],
            features=features))
    return TestSuite(topics=topics)

def list_tests(tests):
    print("# Topics")
    for topic in tests.topics:
        print("## " + topic.description)
        for feature in topic.features:
            s = feature.name
            if feature.description != "":
                s += " (" + feature.description + ")"
            s += ": "
            s += feature.directory
            print(s)
            for test in feature.tests:
                print("* ", test.description)
                print("  ", test.filename)

def run_test(filename):
    result = TestResult()
    r = os.system("lfortran --show-ast --no-color %s &> log.txt" % filename)
    if r == 0:
        result.ast = True
        print("        AST PASS")
    else:
        print("        AST FAIL")
        return result

    r = os.system("lfortran --show-asr --no-color %s &> log.txt" % filename)
    if r == 0:
        result.asr = True
        print("        ASR PASS")
    else:
        print("        ASR FAIL")
        return result

    r = os.system("lfortran --show-llvm --no-color %s &> log.txt" % filename)
    if r == 0:
        result.llvm = True
        print("        LLVM PASS")
    else:
        print("        LLVM FAIL")
        return result

    r = os.system("lfortran -o x.out %s &> log.txt" % filename)
    if r == 0:
        result.bin = True
        print("        BIN PASS")
    else:
        print("        BIN FAIL")
        return result

    r = os.system("./x.out &> log.txt")
    if r == 0:
        result.run = True
        print("        RUN PASS")
    else:
        print("        RUN FAIL")
        return result

    return result

def run_tests(tests):
    print("# Topics")
    for topic in tests.topics:
        print("## " + topic.description)
        for feature in topic.features:
            s = feature.name
            if feature.description != "":
                s += " (" + feature.description + ")"
            s += ": "
            s += feature.directory
            print(s)
            for test in feature.tests:
                print("    ", test.filename)
                test.result = run_test(test.filename)

def get_lfortran_version():
    r = os.system("lfortran --version > log.txt")
    if r == 0:
        return open("log.txt").read()
    else:
        raise Exception("`lfortran --version` failed")

def save_test_results(tests, filename, lfortran_version):
    with open(filename, "w") as f:
        f.write("# Fortran Testsuite\n\n")
        f.write("""\
This page documents what Fortran features are supported by LFortran. For each
feature we list a short description, the filename with the test and current
status for each parts of the compiler:

* AST: The code can be parsed to AST (`lfortran --show-ast test.f90`)
* ASR: The code can be transformed to ASR (`lfortran --show-asr test.f90`)
* LLVM: LFortran can generate LLVM IR (`lfortran --show-llvm test.f90`)
* BIN: The LLVM IR can compile to a binary
* RUN: The binary runs without errors

If all are green it means the feature fully works and you can use it in your
codes. Otherwise you can see what the status is of each feature.

This page is generated automatically using the [Compiler
Tester](https://gitlab.com/lfortran/compiler_tester) repository which contains
all the Fortran tests and scripts to run LFortran to produce the tables below.
We are looking for contributors to contribute more tests. Our goal is to have a
comprehensive Fortran testsuite that can be used to test any Fortran compiler.
\n\n""")
        f.write("Testing the LFortran compiler version:\n\n")
        f.write("```console\n")
        f.write("$ lfortran --version\n")
        f.write(lfortran_version)
        f.write("```\n\n")
        f.write("# Topics\n\n")
        for topic in tests.topics:
            f.write("## " + topic.description + "\n\n")
            for feature in topic.features:
                if feature.description != "":
                    f.write("### " + feature.description + "\n\n")
                else:
                    f.write("### " + feature.name + "\n\n")
                f.write("Directory: `" + feature.directory + "`\n\n")
                f.write("| Description | AST | ASR | LLVM | BIN | RUN | Filename |\n")
                f.write("| ----------- | --- | --- | ---- | --- | --- | -------- |\n")
                for test in feature.tests:
                    filename = os.path.basename(test.filename)
                    link = "https://gitlab.com/lfortran/compiler_tester/-/blob/master/" + test.filename
                    f.write("| ")
                    f.write("`" + test.description + "`")
                    f.write(" | ")
                    if test.result.ast:
                        f.write("✅")
                    else:
                        f.write("❌")
                    f.write("  | ")
                    if test.result.asr:
                        f.write("✅")
                    else:
                        f.write("❌")
                    f.write("  | ")
                    if test.result.llvm:
                        f.write("✅")
                    else:
                        f.write("❌")
                    f.write("   | ")
                    if test.result.bin:
                        f.write("✅")
                    else:
                        f.write("❌")
                    f.write("  | ")
                    if test.result.run:
                        f.write("✅")
                    else:
                        f.write("❌")
                    f.write("  | ")
                    f.write("[%s](%s) |" % (filename, link))
                    f.write("\n")
                f.write("\n\n")


def main():
    parser = argparse.ArgumentParser(description="Fortran Compiler Test Suite")
    parser.add_argument("-l", "--list", action="store_true",
            help="list all tests")
    parser.add_argument("-u", "--update", action="store_true",
            help="update last results")
    args = parser.parse_args()

    d = toml.load(open("tests/tests.toml"))
    print("Collecting tests...")
    tests = collect_tests(d)
    print("    Done.")
    if args.list:
        print("Listing tests")
        list_tests(tests)
        return

    lfortran_version = get_lfortran_version()
    print("Info:")
    print(lfortran_version)

    print("Running tests")
    run_tests(tests)
    print("Done")

    results_file = "results.md"
    if args.update:
        results_file = "last_results.md"

    print("Saving test results to `%s`..." % results_file)
    save_test_results(tests, results_file, lfortran_version)
    print("Done.")

if __name__ == "__main__":
    main()
