# Compiler Tester

Compiler tester automatically tests a given Fortran compiler for features.

Last results: [last_results.md](./last_results.md)

Run tests with:
```
./run_tests.py
```
It will save the report to `results.md`.

The last results can be updated with:
```
./run_tests.py -u
```
This will overwrite `last_results.md`.

# Links

Some related discussions and issues:

* https://gitlab.com/lfortran/lfortran/-/issues/568
* https://github.com/j3-fortran/fortran_proposals/issues/57
