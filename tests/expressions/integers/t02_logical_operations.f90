! logical operations
program t02_logical_operations
implicit none
logical :: a, b

a = .true.
b = .false.

if (a .and. b) then
    print *, "Condition is true"
else
    print *, "Condition is false"
end if

if (a .or. b) then
    print *, "Condition is true"
else
    print *, "Condition is false"
end if

a = .false.
b = .true.

if (.not.(a .and. b)) then
    print *, "Condition is true"
else
    print *, "Condition is false"
end if

if (b .neqv. a) then
    print *, "Condition is true"
else
    print *, "Condition is false"
end if

if (b .eqv. a) then
    print *, "Condition is true"
else
    print *, "Condition is false"
end if

end program t02_logical_operations
