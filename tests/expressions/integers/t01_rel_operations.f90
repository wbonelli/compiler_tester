! relational operations
program t01_rel_operations
implicit none
integer :: a, b

a = 10
b = 20

if (a > b) then
    print *, a, "is greater than", b
else if (a < b) then
    print *, a, "is less than", b
end if

if (a == b) then
    print *, a, "is equal to", b
else if (a /= b) then
    print *, a, "is not equal to", b
end if

if (b .ge. a) then
    print *, a, "is greater than or equal to", b
else if (b .lt. a) then
    print *, a, "is less than", b
else
    print *, a, "is not equal to", b
end if

end program t01_rel_operations
