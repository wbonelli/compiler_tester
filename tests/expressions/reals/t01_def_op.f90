! defined operator
function matmul(mat1, mat2) result (res)
    implicit none

    real, dimension(:,:), intent(in) :: mat1
    real, dimension(:), intent(in)   :: mat2
    real, dimension(size(mat1,1))    :: res

    integer :: i, j
    integer :: n

    n = size(mat2)

    res = 0.0
    do i = 1, n
      res = res + mat2(i) * mat1( :, i )
    end do
end function

program main
implicit none

    interface operator (.matmul.)
        function matmul(mat1, mat2) result (res)
            real, dimension(:,:), intent(in) :: mat1
            real, dimension(:), intent(in)   :: mat2
            real, dimension(size(mat1,1))    :: res
        end function
    end interface

    real, dimension(2, 2) :: mat1
    real, dimension(2)    :: mat2, res
    integer               :: i, j

    call random_number(mat1)
    call random_number(mat2)

    res = mat1 .matmul. mat2

    do i = 1, size(mat1(:, 1))
        write (6, '("[")', advance="no")
        do j = 1, size(mat1(1, :))
            write (6, '(2x f6.4)', advance="no") mat1(i,j)
        end do
        write (6, '("]")', advance="no")

        write (6, '(6x "[" f6.4 "]")', advance="no")   mat2(i)
        write (6, '(6x "[" f6.4 "]")', advance="no")   res(i)
        print *
    end do

end program
