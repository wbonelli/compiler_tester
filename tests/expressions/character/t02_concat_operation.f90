! string concatenation
program concat_operation
implicit none

character(len = 5), parameter :: s1 = "I've "
character(len = 30) :: s2
character(len = 30) :: s3
character(len = 30) :: combined

s2 = "learned from "
s3 = "the best"

combined = s1 // s2 // s3 // "."

print *, combined

end program concat_operation
