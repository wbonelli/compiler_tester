! Basic print
program t01
implicit none
integer :: i
real :: r
i = 5
r = 5.5
print *, "OK"
print *, i
print *, r
print *, .true.
print *, 5
print *, (5.0, 6.0)
end
