! block statement
program block_02
    integer :: a
    a = 100
10   block
        integer :: b
        a = a + 5
        if (a .eq. 105) go to 10
        b = a / 2
        call square(b)
    end block
end program block_02

subroutine square(b)
    integer :: b, result
    result = b * b
    print *, result
end subroutine square
