! go-to statement
program t02
implicit none

    integer, parameter :: a = 10
    go to 1
    print *, "This will be skipped!"
1   print *, a

end program t02
