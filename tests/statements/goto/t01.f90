! computed go-to statement
program t01
implicit none

    integer, parameter :: n = 2
    integer :: a
    a = 10

    go to (1, 2, 3), n
1   a = a + 10
2   a = a + 20
3   a = a + 30
    if(a /= 60) error stop

end program t01
