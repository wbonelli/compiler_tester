! allocate statement
program t01
implicit none

    real, allocatable :: a(:)
    integer :: n, ierr

    n = 2
    allocate(a(n), stat=ierr)

    a = 3
    print *, a
    deallocate(a)

end program t01
