! Basic modules
module A
implicit none
integer :: i
end module

module B
use A, only: i
implicit none
integer :: j
end module

program t01
use B, only: i, j
implicit none
i = 5
j = 6
print *, i, j
end program
