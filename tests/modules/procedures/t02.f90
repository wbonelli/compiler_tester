! Module subroutines
module A
implicit none

contains

    subroutine sub(x, y)
    integer, intent(in) :: x
    integer, intent(out) :: y
    y = x+1
    end subroutine

end module

module B
use A, only: sub
implicit none
end module

program t02
use B, only: sub
implicit none
integer :: i
call sub(5, i)
print *, i
end program
