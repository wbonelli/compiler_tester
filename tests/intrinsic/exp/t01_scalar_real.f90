! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: x4, x4_ref
real(dp) :: x8, x8_ref
x4 = 1._sp
x4 = exp(x4)
x4 = exp(1._sp)
x4_ref = 2.718281828459045_sp
if (abs(x4 - x4_ref) > 1e-5_sp) error stop "exp(1._sp)"

x8 = 1._dp
x8 = exp(x8)
x8 = exp(1._dp)
x8_ref = 2.718281828459045_dp
if (abs(x8 - x8_ref) > 1e-10_dp) error stop "exp(1._dp)"
end program