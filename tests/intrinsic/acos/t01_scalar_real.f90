! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: x4, x4_ref
real(dp) :: x8, x8_ref
x4 = 1._sp
x4 = acos(x4)
x4 = acos(1._sp)
x4_ref = 0.0_sp
if (abs(x4 - x4_ref) > 1e-5_sp) error stop "acos(1._sp)"

x8 = 1._dp
x8 = acos(x8)
x8 = acos(1._dp)
x8_ref = 0.0_dp
if (abs(x8 - x8_ref) > 1e-10_dp) error stop "acos(1._dp)"
end program