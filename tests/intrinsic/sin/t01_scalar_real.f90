! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: x4, x4_ref
real(dp) :: x8, x8_ref
x4 = 1._sp
x4 = sin(x4)
x4 = sin(1._sp)
x4_ref = 0.8414709848078965_sp
if (abs(x4 - x4_ref) > 1e-5_sp) error stop "sin(1._sp)"

x8 = 1._dp
x8 = sin(x8)
x8 = sin(1._dp)
x8_ref = 0.8414709848078965_dp
if (abs(x8 - x8_ref) > 1e-10_dp) error stop "sin(1._dp)"
end program